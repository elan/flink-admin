package cn.chongho.inf.flink.request;

import lombok.Data;

/**
 * @author ming
 */
@Data
public class JarListRequest extends BaseParameters{

    private String fileName = "";

}
